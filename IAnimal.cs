﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GodGame
{
    public interface IAnimal 
    {
        void SeDeplacer();
        void SeReproduire();
    }
}