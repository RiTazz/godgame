﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GodGame
{
    public interface IBacterie
    {
        void SeDeplacer();
        void SeDiviser();
    }
}