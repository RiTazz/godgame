﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GodGame
{
    public interface IVegetal
    {
        void SeDiviser();
        void SeReproduire();
    }
}